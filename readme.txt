ELEVATOR TASK (Author: Mikita Isakau)

Description:

    This program simulates the operation of the elevator
    in an apartment building. It uses a multi-threaded programming
    concepts using java.util.concurrent package

Requirements:
    - Gradle v3.2.0 and above
    - Strait hands growing from the right place

MANUAL FOR RUN APPLICATION:
    To run this application, go to the root directory and
    enter the command "gradle run"

TEST RUN:
    To run this application's test, please go to the root directory and
    enter the command "gradle test"

Have a nice day! :)