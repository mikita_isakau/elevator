package by.epam.training;

import by.epam.training.beans.AppLogger;
import by.epam.training.beans.Floor;
import by.epam.training.beans.House;
import by.epam.training.beans.Passenger;
import by.epam.training.exceptions.ElevatorTaskBuilderException;
import by.epam.training.loaders.Loader;
import by.epam.training.loaders.PropertiesLoader;
import by.epam.training.builders.ElevatorTaskBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

/**
 * Package: by.epam.training
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 06.01.2017
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { Config.class })
public class ElevatorTaskBuilderTest {

    @Test
    public void latch() throws Exception {

    }

    @Test
    public void getTransportationTaskList() throws Exception {

    }

    @Test
    public void getElevator() throws Exception {

    }

    @Test
    public void getController() throws Exception {

    }

    @Test
    public void getRandomPassengers() throws Exception {

    }

    @Test
    public void getFloors() throws Exception {

    }

    @Test
    public void getHouse() throws Exception {

    }
}
