package by.epam.training.beans;

import by.epam.training.constants.Direction;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.is;

/**
 * Package: by.epam.training.constants
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 13.01.2017
 */
public class DirectionTest {

    @Test
    public void testValueOfInDirection()
    {
        Floor firstFloor = Mockito.mock(Floor.class);
        Floor secondFloor = Mockito.mock(Floor.class);
        Direction resultDirection = Direction.valueOf(firstFloor, secondFloor);
        Assert.assertThat("The current direction is: ", resultDirection, is(Direction.UP));
    }

    @Test
    public void testSwitchDirection() {
        Direction oppositeDirection = Direction.switchDirection(Direction.DOWN);
        Assert.assertThat("The opposite DOWN direction is", oppositeDirection, is(Direction.UP));
    }

}
