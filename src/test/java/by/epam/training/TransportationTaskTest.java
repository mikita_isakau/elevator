package by.epam.training;

import by.epam.training.beans.AppLogger;
import by.epam.training.beans.Elevator;
import by.epam.training.beans.House;
import by.epam.training.beans.Passenger;
import by.epam.training.controllers.ElevatorController;
import by.epam.training.loaders.Loader;
import by.epam.training.loaders.PropertiesLoader;
import by.epam.training.builders.ElevatorTaskBuilder;
import by.epam.training.tasks.TransportationTask;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Package: by.epam.training
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 14.01.2017
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { Config.class })
public class TransportationTaskTest {

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private ElevatorController controller;

    @Autowired
    private CountDownLatch latch;

    @Autowired
    private House testHouse;

    @Autowired
    private Elevator elevator;

    @Autowired
    private List<TransportationTask> tasks;

    @Autowired
    private Loader xmlLoader;

    @Mock
    private AppLogger appLogger;

    private TransportationTask task;


    @Before
    public void init() {
        appLogger = mock(AppLogger.class);
        doNothing().when(appLogger).log(anyString());
    }

    @Test
    public void testCorrectStatementBeforeTransportation() {
        List<Passenger> passengers = testHouse.getPassengers();
        for (Passenger passenger : passengers) {
            if (passenger.getStatement() != Passenger.Statement.NOT_STARTED) {
                Assert.fail();
            }
        }
    }

    @Test
    public void checkTransportationTasksHasPassengersFromHouse() {
        List<Passenger> passengersFromTasks = new ArrayList<>();
        for (TransportationTask task : tasks) {
            passengersFromTasks.add(task.getPassenger());
        }
        Collections.sort(passengersFromTasks);
        Collections.sort(testHouse.getPassengers());
        Assert.assertThat("The passenger is all in tasks", passengersFromTasks, is(testHouse.getPassengers()));
    }

    @Test(timeout = 11000)
    public void testCorrectStatementAfterTransportation() throws InterruptedException {
        executorService.submit(task);
        executorService.awaitTermination(10, TimeUnit.SECONDS);
        Assert.assertThat("The current statement is ",
                task.getPassenger().getStatement(),
                is(Passenger.Statement.COMPLETED));
    }

    @Test(timeout = 11000)
    public void testCorrectFloorDeboading() throws InterruptedException {
        executorService.submit(task);
        executorService.awaitTermination(10, TimeUnit.SECONDS);
        Assert.assertTrue(task.getPassenger()
                .getDestinationFloor()
                .getArrivalContainer()
                .isContains(task.getPassenger()));
    }

    @Test(timeout = 11000)
    public void testEmptyPassengerAfterTransportation() throws InterruptedException {
        executorService.submit(task);
        executorService.awaitTermination(10, TimeUnit.SECONDS);
        Assert.assertTrue(!task.getPassenger()
                .getSourceFloor()
                .getDispatchContainer()
                .isContains(task.getPassenger()));
    }


    @After
    public void destroy() {
        executorService.shutdown();
    }
}
