package by.epam.training;

import by.epam.training.loaders.Loader;
import by.epam.training.loaders.PropertiesLoader;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.MissingResourceException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;

/**
 * Package: by.epam.training
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 05.01.2017
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { Config.class })
public class PropertiesLoaderTest {

    @Autowired
    private Loader loader;

    @Test
    public void testLoadConfig()
    {
        Assert.assertThat("Config not null", loader, notNullValue());
    }

    @Test
    public void testRightValuePassengersInConfig() {
        Assert.assertThat("The passengers number is", loader.getPassengersNumber(), is(10));
    }

    @Test
    public void testRightValueElevatorCapacityInConfig() {
        Assert.assertThat("The elevator capacity is", loader.getElevatorCapacity(), is(10));
    }

    @Test
    public void testRightValueStoriesInConfig() {
        Assert.assertThat("The stories number is", loader.getStoriesNumber(), is(10));
    }

    @Test
    public void testEqualsLoaderConfig() {
        Loader loader = new PropertiesLoader(10,10,10);
        Assert.assertThat("The loader ", this.loader, is(loader));
    }

}

