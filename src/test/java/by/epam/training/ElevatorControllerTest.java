package by.epam.training;

import by.epam.training.beans.*;
import by.epam.training.constants.Direction;
import by.epam.training.controllers.ElevatorController;
import by.epam.training.exceptions.ElevatorControllerException;
import by.epam.training.loaders.Loader;
import by.epam.training.tasks.TransportationTask;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Package: by.epam.training
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 14.01.2017
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/beans.xml")
public class ElevatorControllerTest {

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private ElevatorController controller;

    @Autowired
    private CountDownLatch latch;

    @Autowired
    private House house;

    @Autowired
    private Elevator elevator;

    @Autowired
    private List<TransportationTask> transportationTasks;

    @Mock
    private AppLogger appLogger;

    @Autowired
    private AppLogger logger;

    private Floor firstFloor;

    private Floor lastFloor;

    @Before
    public void initService() {
        appLogger = mock(AppLogger.class);

        doAnswer((invocation) -> {
            System.out.println(invocation.getArguments()[0]);
            return null;
        }).when(appLogger).log(anyString());

        final int lastFloorNumber = house.getFloors().size() - 1;

        firstFloor = house.getFloors().get(0);
        lastFloor = house.getFloors().get(lastFloorNumber);
    }

    @Test
    public void testMoveElevatorWithOnePassenger() {

    }


    @After
    public void destroy() {
        executorService.shutdownNow();
    }
}
