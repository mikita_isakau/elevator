package by.epam.training.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Package: by.epam.training
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 14.01.2017
 */

@Component("logger")
public class AppLogger {

    private static final String DEFAULT_CLASS_NAME = "by.epam.training.AppLogger";
    private Logger logger;
    private final Lock lock = new ReentrantLock(true);

    public AppLogger() {
        this(DEFAULT_CLASS_NAME);
    }

    public AppLogger(@Value("${logger.className}") String className) {
        try {
            logger = LogManager.getLogger(Class.forName(className));
        } catch (ClassNotFoundException e) {
            logger = LogManager.getLogger();
        }
    }

    public void log(String message){
        lock.lock();
        try {
            logger.info(message);
        } finally {
            lock.unlock();
        }
    }

    public void error(String message){
        lock.lock();
        try {
            logger.error(message);
        } finally {
            lock.unlock();
        }
    }

    public final void error(String message, Throwable cause){
        lock.lock();
        try {
            logger.error(message, cause);
        } finally {
            lock.unlock();
        }
    }

}
