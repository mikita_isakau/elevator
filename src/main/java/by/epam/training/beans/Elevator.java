package by.epam.training.beans;

import by.epam.training.constants.Direction;
import by.epam.training.containers.AbstractContainer;
import by.epam.training.containers.ElevatorContainer;
import by.epam.training.loaders.Loader;

import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Package: by.epam.training.beans
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 06.01.2017
 */
public class Elevator {

    private int elevatorStories;

    private AtomicReference<Floor> currentFloor;
    private AtomicReference<Direction> currentDirection;
    private ElevatorContainer elevatorContainer;

    private Lock joinLock = new ReentrantLock();
    private Lock exitLock = new ReentrantLock();

    private Condition joinCondition = joinLock.newCondition();
    private Condition exitCondition = exitLock.newCondition();

    public Lock getJoinLock() {
        return joinLock;
    }

    public Lock getExitLock() {
        return exitLock;
    }

    public Condition getJoinCondition() {
        return joinCondition;
    }

    public Condition getExitCondition() {
        return exitCondition;
    }

    public Elevator(Loader loader) {
        this.elevatorStories = loader.getElevatorCapacity();
        this.currentFloor = new AtomicReference<>();
        this.currentDirection = new AtomicReference<>();
        this.elevatorContainer = new ElevatorContainer(this.elevatorStories);
    }

    public int getElevatorStories() {
        return elevatorStories;
    }

    public Floor getCurrentFloor() {
        return currentFloor.get();
    }

    public Elevator setCurrentFloor(Floor currentFloor) {
        this.currentFloor.set(currentFloor);
        return this;
    }

    public Direction getCurrentDirection() {
        return currentDirection.get();
    }

    public Elevator setCurrentDirection(Direction currentDirection) {
        this.currentDirection.set(currentDirection);
        return this;
    }

    public List<Passenger> getElevatorContainer() {
        return elevatorContainer.passengersList();
    }

    public Elevator addInContainer(Passenger passenger) {
        elevatorContainer.put(passenger);
        return this;
    }

    public boolean removeFromContainer(Passenger passenger) {
        return elevatorContainer.remove(passenger);
    }

    public boolean isContains(Passenger passenger) {
        return elevatorContainer.isContains(passenger);
    }

    public boolean isFullElevator() {
        return elevatorContainer.size() == elevatorStories;
    }
}
