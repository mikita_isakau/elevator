package by.epam.training.beans;

import by.epam.training.constants.Direction;

import java.util.Objects;

/**
 * Package: by.epam.training.beans
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 04.01.2017
 */
public class Passenger implements Comparable<Passenger>{

    public enum Statement {
        NOT_STARTED,
        IN_PROGRESS,
        COMPLETED
    }

    private Statement statement = Statement.NOT_STARTED;

    private final long ID;

    private Floor sourceFloor;

    private Floor destinationFloor;

    public Passenger setDirection(Direction direction) {
        this.direction = direction;
        return this;
    }

    private Direction direction;

    public Passenger(long id, Floor sourceFloor, Floor destinationFloor) {
        this.ID = id;
        setSourceFloor(sourceFloor);
        setDestinationFloor(destinationFloor);
        this.direction = Direction.valueOf(getSourceFloor(), getDestinationFloor());
    }

    public Passenger(long id) {
        super();
        this.ID = id;
    }

    public long getID() {
        return ID;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public Floor getSourceFloor() {
        return sourceFloor;
    }

    public void setSourceFloor(Floor sourceFloor) {
        this.sourceFloor = sourceFloor;
    }

    public Floor getDestinationFloor() {
        return destinationFloor;
    }

    public void setDestinationFloor(Floor destinationFloor) {
        this.destinationFloor = destinationFloor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Passenger)) return false;
        Passenger passenger = (Passenger) o;
        return ID == passenger.ID;
    }

    @Override
    public int compareTo(Passenger o) {
        return (int) (getID() - o.ID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getID());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Passenger{");
        sb.append("statement=").append(statement);
        sb.append(", ID=").append(ID);
        sb.append(", sourceFloor=").append(sourceFloor.getFloorNumber());
        sb.append(", destinationFloor=").append(destinationFloor.getFloorNumber());
        sb.append(", direction =").append(direction);
        sb.append('}');
        return sb.toString();
    }
}
