package by.epam.training.beans;


import by.epam.training.containers.AbstractContainer;
import by.epam.training.containers.FloorContainer;

import java.util.Objects;

/**
 * Package: by.epam.training.beans
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 04.01.2017
 */
public class Floor implements Comparable<Floor>{

    private int floorNumber;

    private final AbstractContainer dispatchStoryContainer = new FloorContainer();
    private final AbstractContainer arrivalStoryContainer = new FloorContainer();

    public Floor(int floorNumber) {
        this.setFloorNumber(floorNumber);
    }

    @Override
    public int compareTo(Floor o) {
        return this.floorNumber - o.getFloorNumber();
    }

    public void setFloorNumber(int floorNumber) {
        this.floorNumber = floorNumber;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public void addToDispatchContainer(Passenger passenger) {
        dispatchStoryContainer.put(passenger);
    }

    public void removeFromDispatchContainer(Passenger passenger) {
        dispatchStoryContainer.remove(passenger);
    }

    public void addToArrivalContainer(Passenger passenger) {
        arrivalStoryContainer.put(passenger);
    }

    public boolean isEmptyDispatch(){
        return dispatchStoryContainer.isEmpty();
    }

    public int sizeDispatch() {
        return dispatchStoryContainer.size();
    }

    public int sizeArrival() {
        return arrivalStoryContainer.size();
    }

    public AbstractContainer getDispatchContainer() {
        return dispatchStoryContainer;
    }

    public AbstractContainer getArrivalContainer() {
        return arrivalStoryContainer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Floor)) return false;
        Floor floor = (Floor) o;
        return getFloorNumber() == floor.getFloorNumber();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFloorNumber());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Floor{");
        sb.append("floorNumber=").append(floorNumber);
        sb.append(", dispatchCount=").append(dispatchStoryContainer.size());
        sb.append(", dispatchStoryContainer=").append(dispatchStoryContainer);
        sb.append(", arrivalCount=").append(arrivalStoryContainer.size());
        sb.append(", arrivalStoryContainer=").append(arrivalStoryContainer);
        sb.append('}');
        return sb.toString();
    }
}
