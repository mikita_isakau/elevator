package by.epam.training.beans;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * Package: by.epam.training.beans
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 06.01.2017
 */

public class House {

    private List<Floor> floors;
    private Elevator elevator;
    private List<Passenger> passengers;

    public House() { }

    public Elevator getElevator() {
        return elevator;
    }

    public House setElevator(Elevator elevator) {
        this.elevator = elevator;
        return this;
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public House setFloors(List<Floor> floors) {
        this.floors = floors;
        return this;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public House setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
        return this;
    }

    public static void verifyHouse(House house) {
        if (Objects.isNull(house.floors) || Objects.isNull(house.passengers) || Objects.isNull(house.elevator)) {
            throw new NullPointerException();
        }
        if ((!house.floors.isEmpty() && house.floors.size() > 1) &&
                (!house.passengers.isEmpty() && house.passengers.size() >= 1)) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("House{");
        sb.append("floors=").append(floors);
        sb.append(", elevator=").append(elevator);
        sb.append(", passengers=").append(passengers);
        sb.append('}');
        return sb.toString();
    }
}
