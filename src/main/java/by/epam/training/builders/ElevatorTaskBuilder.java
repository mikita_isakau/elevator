package by.epam.training.builders;

import by.epam.training.beans.AppLogger;
import by.epam.training.beans.Elevator;
import by.epam.training.beans.Floor;
import by.epam.training.beans.House;
import by.epam.training.beans.Passenger;
import by.epam.training.constants.Direction;
import by.epam.training.controllers.ElevatorController;
import by.epam.training.exceptions.ElevatorTaskBuilderException;
import by.epam.training.loaders.Loader;
import by.epam.training.tasks.TransportationTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.annotation.*;

import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * Package: by.epam.training.builders
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 05.01.2017
 */

@Configuration
public class ElevatorTaskBuilder {

    private AppLogger logger;
    private Loader loader;

    @Autowired
    public ElevatorTaskBuilder(Loader loader, AppLogger logger) {
        verify(loader, logger);
        this.loader = loader;
        this.logger = logger;
    }

    @Bean
    @Required
    @DependsOn({"loader"})
    public CountDownLatch latch(Loader loader) {
       return new CountDownLatch(loader.getPassengersNumber());
    }

    @Bean("transportationTasks")
    @Required
    @Scope("prototype")
    @DependsOn({"controller", "passengers", "latch"})
    public List<TransportationTask> getTransportationTaskList(ElevatorController controller,
                                                              List<Passenger> passengers,
                                                              CountDownLatch latch) {
        List<TransportationTask> tasks = new ArrayList<>();
        for (Passenger passenger : passengers) {
            TransportationTask task = new TransportationTask(controller, passenger, latch);
            tasks.add(task);
        }
        return tasks;
    }

    @Bean("elevator")
    @Required
    @DependsOn({"floors"})
    public Elevator getElevator(List<Floor> floors) {
        if (Objects.isNull(floors) || floors.isEmpty()) {
            throw new ElevatorTaskBuilderException("Floors shouldn't be a null or empty");
        }
        Elevator elevator = new Elevator(loader);
        elevator.setCurrentDirection(Direction.UP)
                .setCurrentFloor(floors.get(0));
        return elevator;
    }

    @Bean("controller")
    @Required
    @DependsOn({"latch", "elevator", "floors"})
    public ElevatorController getController(CountDownLatch latch, Elevator elevator, List<Floor> floors) {
        ElevatorController controller = new ElevatorController();
        controller.setElevator(elevator)
                .setLogger(logger)
                .setFloors(floors)
                .setCount(loader.getPassengersNumber());
        return controller;
    }

    @Bean("passengers")
    @Required
    @DependsOn({"floors"})
    public List<Passenger> getRandomPassengers(List<Floor> floors) {
        if (loader.getPassengersNumber() <= 0) {
            throw new ElevatorTaskBuilderException("The passengerCount " +
                    "should have a positive number count");
        }
        Set<Passenger> passengerSet = new HashSet<>(loader.getPassengersNumber());
        List<Long> randomNumbers = randomIDs(loader.getPassengersNumber());

        for (int i = 0; i < loader.getPassengersNumber(); i++) {
            passengerSet.add(new Passenger(randomNumbers.get(i)));
        }
        List<Passenger> passengerList = new ArrayList<>(passengerSet);
        generateFloorsForPassengers(floors, passengerList);
        movePassengersToDispatchStoryContainer(passengerList, floors);
        return passengerList;
    }

    @Bean("floors")
    @Required
    @DependsOn({"loader"})
    public List<Floor> getFloors(Loader loader){
        Set<Floor> newFloors = new TreeSet<>();
        for (int i = 0; i < loader.getStoriesNumber(); i++) {
            newFloors.add(new Floor(i));
        }
        return new ArrayList<>(newFloors);
    }

    @Bean("house")
    @Required
    @DependsOn({"elevator", "floors", "passengers"})
    public House getHouse(Elevator elevator, List<Floor> floors, List<Passenger> passengers) {
        return new House()
                .setElevator(elevator)
                .setFloors(floors)
                .setPassengers(passengers);
    }

    private void generateFloorsForPassengers(List<Floor> floors, List<Passenger> passengers) {
        Optional<List<Floor>> listOptionalFloors = Optional.ofNullable(floors);
        Optional<List<Passenger>> listOptionalPassengers = Optional.ofNullable(passengers);
        if (Objects.isNull(floors)) {
            throw new ElevatorTaskBuilderException("Argument floors shouldn't be a null!",
                    new NullPointerException());
        }
        if (Objects.isNull(passengers)) {
            throw new ElevatorTaskBuilderException("Argument passengers shouldn't be a null!",
                    new NullPointerException());
        }
        for (Passenger passenger : passengers) {
            Floor source = floors.get(generateInt(loader.getStoriesNumber()));
            passenger.setSourceFloor(source);
            passenger.setDestinationFloor(generateDestinationFloor(floors, passenger.getSourceFloor()));
            passenger.setDirection(Direction.valueOf(passenger.getSourceFloor(), passenger.getDestinationFloor()));
        }
    }

    private void verify(Loader loader, AppLogger logger) {
        if (Objects.isNull(loader)) {
            throw new ElevatorTaskBuilderException("Loader shouldn't be a null");
        }
        if (Objects.isNull(logger)) {
            throw new ElevatorTaskBuilderException("AppLogger shouldn't be a null");
        }
        if (loader.getStoriesNumber() <= 1 ||
                loader.getElevatorCapacity() <= 0 ||
                loader.getPassengersNumber() <= 0) {
            throw new ElevatorTaskBuilderException("Wrong Loader's parameters");
        }
    }

    private static Floor generateDestinationFloor(List<Floor> listFloor, Floor sourceFloor){

        if (Objects.isNull(listFloor)) {
            throw new ElevatorTaskBuilderException("Floors shouldn't have a null!");
        }
        if (Objects.isNull(sourceFloor)) {
            throw new ElevatorTaskBuilderException("Source floor shouldn't have a null!");
        }

        Floor destinationFloor =
                listFloor.get(generateInt(listFloor.size()));
        while (destinationFloor.equals(sourceFloor)) {
            destinationFloor =
                    listFloor.get(generateInt(listFloor.size()));
        }
        return destinationFloor;
    }

    private static List<Long> randomIDs(int count) {
        Set<Long> randomSet = new HashSet<>(count,1f);
        while (randomSet.size() < count) {
            randomSet.add((long) generateInt(Short.MAX_VALUE));
        }
        return new ArrayList<>(randomSet);
    }

    private static int generateInt(int size) {
        return (int) (Math.random() * size);
    }

    private void movePassengersToDispatchStoryContainer(List<Passenger> passengers, List<Floor> floors) {
        if (Objects.isNull(passengers)) {
            throw new ElevatorTaskBuilderException("passengers shouldn't be a null",
                    new NullPointerException());
        }
        if (Objects.isNull(floors)) {
            throw new ElevatorTaskBuilderException("floors shouldn't be a null",
                    new NullPointerException());
        }

        for (Passenger passenger : passengers) {
            if (floors.contains(passenger.getSourceFloor())) {
                floors.get(floors.indexOf(passenger.getSourceFloor()))
                        .addToDispatchContainer(passenger);
            }
        }
    }
}
