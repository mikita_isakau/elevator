package by.epam.training.constants;

import by.epam.training.beans.Floor;

/**
 * Package: by.epam.training.constants
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 06.01.2017
 */
public enum Direction {

    UP, DOWN;

    public static Direction valueOf(Floor sourceFloor, Floor destinationFloor){
        if (sourceFloor.compareTo(destinationFloor) > 0){
            return Direction.DOWN;
        } else if (sourceFloor.compareTo(destinationFloor) < 0) {
            return Direction.UP;
        }
        return null;
    }

    public static Direction switchDirection(Direction direction) {
        return direction == Direction.UP ? Direction.DOWN : Direction.UP;
    }
}
