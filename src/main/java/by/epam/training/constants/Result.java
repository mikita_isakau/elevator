package by.epam.training.constants;

/**
 * Package: by.epam.training.constants
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 15.01.2017
 */
public enum Result {
    SUCCESSFULL, FAILTURE
}
