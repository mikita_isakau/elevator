package by.epam.training.containers;

import by.epam.training.beans.Passenger;

/**
 * Package: by.epam.training.containers
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 27.01.2017
 */
public class ElevatorContainer extends AbstractContainer {

    private int capacity;

    public ElevatorContainer(int capacity) {
        super();
        this.capacity = capacity;
    }

    public boolean isFull() {
        return size() == capacity;
    }

    @Override
    public void put(Passenger passenger) {
        if (isFull()) {
            throw new IllegalArgumentException("The ElevatorContainer is full!");
        }
        super.put(passenger);
    }
}
