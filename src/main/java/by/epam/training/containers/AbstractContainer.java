package by.epam.training.containers;

import by.epam.training.beans.Passenger;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Package: by.epam.training.containers
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 27.01.2017
 */
public abstract class AbstractContainer {

    private List<Passenger> passengerList = new CopyOnWriteArrayList<>();

    public AbstractContainer() { }

    public int size() {
        return passengerList.size();
    }

    public void put(Passenger passenger) {
        passengerList.add(passenger);
    }

    public boolean remove(Passenger passenger){
        return passengerList.remove(passenger);
    }

    public boolean isContains(Passenger passenger) {
        return passengerList.contains(passenger);
    }

    public boolean isEmpty() {
        return passengerList.isEmpty();
    }

    public List<Passenger> passengersList(){
        return Collections.unmodifiableList(passengerList);
    }
}
