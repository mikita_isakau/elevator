package by.epam.training;

import org.springframework.context.annotation.*;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Package: by.epam.training
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 08.02.2017
 */
@Configuration
@ComponentScan("by.epam.training")
@PropertySource(value = "config.properties",
        encoding = "UTF-8",
        ignoreResourceNotFound = true)
public class Config {
    @Bean
    @Lazy
    @Scope("prototype")
    public ExecutorService executorService() {
        return Executors.newCachedThreadPool();
    }
}
