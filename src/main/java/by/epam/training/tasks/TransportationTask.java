package by.epam.training.tasks;

import by.epam.training.beans.Elevator;
import by.epam.training.beans.Passenger;
import by.epam.training.constants.Result;
import by.epam.training.controllers.ElevatorController;
import by.epam.training.exceptions.TransportationTaskException;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * Package: by.epam.training.tasks
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 05.01.2017
 */

public class TransportationTask implements Callable<Result> {

    private final Passenger passenger;
    private final CountDownLatch latch;
    private final ElevatorController controller;

    public TransportationTask(ElevatorController controller, Passenger passenger,
                              CountDownLatch latch) {
        if (Objects.isNull(passenger)) {
            throw new TransportationTaskException("The passenger shouldn't be a null");
        }
        this.passenger = passenger;
        this.controller = controller;
        this.latch = latch;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    @Override
    public String toString() {
        return passenger.toString();
    }

    @Override
    public Result call() throws Exception {
        Elevator elevator = controller.getElevator();
        final Lock joinLock = elevator.getJoinLock();
        final Lock exitLock = elevator.getExitLock();
        final Condition joinCondition = elevator.getJoinCondition();
        final Condition exitCondition = elevator.getExitCondition();
        latch.countDown();

        if (!passenger.getSourceFloor().getDispatchContainer().isContains(passenger)) {
            throw new TransportationTaskException("The passenger not contains " +
                        "in dispatcher container");
        }
        joinLock.lock();
        try {
            while (!Objects.equals(elevator.getCurrentFloor(), passenger.getSourceFloor()) ||
                    elevator.isFullElevator()) {
                joinCondition.await();
            }
            passenger.getSourceFloor().removeFromDispatchContainer(passenger);
            controller.joinPassenger(passenger);
        } finally {
            joinCondition.signalAll();
            joinLock.unlock();
        }

        passenger.setStatement(Passenger.Statement.IN_PROGRESS);

        exitLock.lock();
        try {
            while (!Objects.equals(elevator.getCurrentFloor(), passenger.getDestinationFloor())) {
                exitCondition.await();
            }
            controller.exitPassenger(passenger);
            passenger.getDestinationFloor().addToArrivalContainer(passenger);
            if (!passenger.getDestinationFloor().getArrivalContainer().isContains(passenger)) {
                throw new TransportationTaskException("The passenger not contains " +
                        "in arrival container");
            }
            passenger.setStatement(Passenger.Statement.COMPLETED);
        } finally {
            exitCondition.signalAll();
            exitLock.unlock();
        }
        return Result.SUCCESSFULL;
    }
}
