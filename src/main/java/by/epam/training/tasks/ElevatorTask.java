package by.epam.training.tasks;

import by.epam.training.beans.AppLogger;
import by.epam.training.constants.Result;
import by.epam.training.controllers.ElevatorController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 * Package: by.epam.training.tasks
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 25.02.2017
 */

@Component
@DependsOn({"controller", "latch", "logger"})
public class ElevatorTask
        implements Callable<Result>  {

    private final ElevatorController controller;
    private final CountDownLatch latch;
    private final AppLogger logger;

    @Autowired
    public ElevatorTask(ElevatorController controller, CountDownLatch latch, AppLogger logger) {
       this.controller = controller;
       this.latch = latch;
       this.logger = logger;
    }

    @Override
    public Result call() throws Exception {
        latch.await();
        logger.log("STARTING_TRANSPORTATION");

        while (controller.getCount() > 0) {
            controller.elevatorAction();
            controller.moveOneStepFloor();
        }
        return Result.SUCCESSFULL;
    }
}
