package by.epam.training.loaders;

import org.springframework.context.annotation.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * Package: by.epam.training.loaders
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 19.01.2017
 */

@Configuration("xmlLoader")
public class XMLConfigLoader extends AbstractLoader {

    private enum Elements {
        CONFIG, ELEVATOR, PASSENGERS, FLOORS
    }

    private static final String DEFAULT_CONFIG = "src/main/resources/config.xml";

    public XMLConfigLoader() throws ParserConfigurationException, SAXException, IOException {
        this(DEFAULT_CONFIG);
    }

    public XMLConfigLoader(final String filename) throws IOException, SAXException, ParserConfigurationException {
        final File xmlFile = new File(filename);
        final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        final Document document = documentBuilder.parse(xmlFile);
        document.getDocumentElement().normalize();

        final Element config = document.getDocumentElement();
        if (!Elements.CONFIG.name().equalsIgnoreCase(config.getNodeName())) { throw new IllegalArgumentException(); }
        NodeList list = config.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            final Node node = list.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                switch (Elements.valueOf(node.getNodeName().toUpperCase())) {
                    case ELEVATOR: {
                        final String value = node.getAttributes().getNamedItem("capacity").getNodeValue();
                        this.setElevatorCapacity(Integer.valueOf(value));
                    } break;
                    case PASSENGERS: {
                        final String value = node.getAttributes().getNamedItem("count").getNodeValue();
                        this.setPassengersNumber(Integer.valueOf(value));
                    } break;
                    case FLOORS: {
                        final String value = node.getAttributes().getNamedItem("count").getNodeValue();
                        this.setStoriesNumber(Integer.valueOf(value));
                    } break;
                }
            }
        }
    }
}
