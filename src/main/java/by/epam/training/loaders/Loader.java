package by.epam.training.loaders;

/**
 * Package: by.epam.training.loaders
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 19.01.2017
 */
public interface Loader {
    int getStoriesNumber();
    int getElevatorCapacity();
    int getPassengersNumber();
}
