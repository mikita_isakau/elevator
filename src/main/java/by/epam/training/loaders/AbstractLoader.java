package by.epam.training.loaders;

import java.util.Objects;

/**
 * Package: by.epam.training.loaders
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 18.02.2017
 */

public abstract class AbstractLoader implements Loader {

    private int storiesNumber;
    private int elevatorCapacity;
    private int passengersNumber;

    public AbstractLoader() { }

    @Override
    public int getStoriesNumber() {
        return storiesNumber;
    }

    @Override
    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    @Override
    public int getPassengersNumber() {
        return passengersNumber;
    }

    public AbstractLoader setStoriesNumber(int storiesNumber) {
        this.storiesNumber = storiesNumber;
        return this;
    }

    public AbstractLoader setElevatorCapacity(int elevatorCapacity) {
        this.elevatorCapacity = elevatorCapacity;
        return this;
    }

    public AbstractLoader setPassengersNumber(int passengersNumber) {
        this.passengersNumber = passengersNumber;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractLoader)) return false;
        AbstractLoader that = (AbstractLoader) o;
        return getStoriesNumber() == that.getStoriesNumber() &&
                getElevatorCapacity() == that.getElevatorCapacity() &&
                getPassengersNumber() == that.getPassengersNumber();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStoriesNumber(), getElevatorCapacity(), getPassengersNumber());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Loader{");
        sb.append("storiesNumber=").append(storiesNumber);
        sb.append(", elevatorCapacity=").append(elevatorCapacity);
        sb.append(", passengersNumber=").append(passengersNumber);
        sb.append('}');
        return sb.toString();
    }
}
