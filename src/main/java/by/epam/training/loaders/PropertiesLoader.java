package by.epam.training.loaders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;


/**
 * Package: by.epam.training.loaders
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 04.01.2017
 */

@Configuration("loader")
public class PropertiesLoader extends AbstractLoader {
    
    public PropertiesLoader(@Value("${elevator.stories_count}") int storiesNumber,
                            @Value("${elevator.capacity}") int elevatorCapacity,
                            @Value("${elevator.passengers_count}") int passengersNumber){
            this
                .setPassengersNumber(passengersNumber)
                .setElevatorCapacity(elevatorCapacity)
                .setStoriesNumber(storiesNumber);
    }
}
