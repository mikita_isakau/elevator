package by.epam.training.controllers;

import by.epam.training.beans.AppLogger;
import by.epam.training.beans.Elevator;
import by.epam.training.beans.Floor;
import by.epam.training.beans.Passenger;
import by.epam.training.constants.Direction;
import by.epam.training.exceptions.ElevatorControllerException;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * Package: by.epam.training.controllers
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 04.01.2017
 */

public class ElevatorController {
    private AppLogger logger;
    private Elevator elevator;
    private CountDownLatch latch;
    private List<Floor> floors;
    private List<Passenger> elevatorContainer;

    private volatile int count = 0;

    public ElevatorController() {
        super();
    }

    public ElevatorController setElevator(Elevator elevator) {
        if (Objects.isNull(elevator)) {
            throw new ElevatorControllerException("Elevator shouldn't be a null");
        }
        this.elevator = elevator;
        elevatorContainer = elevator.getElevatorContainer();
        return this;
    }

    public ElevatorController setFloors(List<Floor> floors) {
        if (Objects.isNull(floors)) {
            throw new ElevatorControllerException("Floors shouldn't be a null");
        }
        this.floors = floors;
        return this;
    }

    public ElevatorController setLogger(AppLogger logger) {
        this.logger = logger;
        return this;
    }

    public ElevatorController setCount(int count) {
        this.count = count;
        return this;
    }

    public int getCount() {
        return count;
    }

    public Elevator getElevator() {
        return elevator;
    }

    public void joinPassenger(Passenger passenger) {
        Elevator elevator = getElevator();
        if (elevator.isContains(passenger)) {
            throw new ElevatorControllerException("The passenger inside elevator");
        }
        elevator.addInContainer(passenger);
        logger.log("BOADING OF PASSENGER ("+ passenger.getID() + " ON "
                + elevator.getCurrentFloor().getFloorNumber() + " STORY)");
    }

    public void exitPassenger(Passenger passenger) {
        Elevator elevator = getElevator();
        if (!elevator.isContains(passenger)) {
            throw new ElevatorControllerException("The passenger not inside elevator");
        }
        elevator.removeFromContainer(passenger);
        logger.log("DEBOADING OF PASSENGER ("+ passenger.getID() + " ON "
                    + elevator.getCurrentFloor().getFloorNumber() + " STORY)");
        latch.countDown();
        count--;
    }

    public void moveOneStepFloor() {
        if (!floors.contains(elevator.getCurrentFloor())) {
            throw new ElevatorControllerException("Error in current floor");
        }
        int currentFloorNumber = elevator.getCurrentFloor().getFloorNumber();
        int oldFloorNumber = currentFloorNumber;
        Direction direction = elevator.getCurrentDirection();
        switch (direction){
            case UP: {
                elevator.setCurrentFloor(floors.get(++currentFloorNumber));
            } break;
            case DOWN: {
                elevator.setCurrentFloor(floors.get(--currentFloorNumber));
            } break;
        }
        if (currentFloorNumber == 0 ||
                currentFloorNumber == (floors.size() - 1)) {
            elevator.setCurrentDirection(Direction.switchDirection(direction));
        }
        logger.log("MOVING_ELEVATOR (" + oldFloorNumber + "," + currentFloorNumber +
                 ") " + direction + " Count passenger in elevator: " + elevatorContainer.size());
    }

    private CountDownLatch generateLatch(List<Passenger> passengers) {
        int countLatch = 0;
        for (Passenger passenger : passengers) {
            if (passenger.getDestinationFloor() == getElevator().getCurrentFloor()) {
                countLatch++;
            }
        }
        return new CountDownLatch(countLatch);
    }

    public void elevatorAction() throws InterruptedException {
        Lock joinLock = elevator.getJoinLock();
        Lock exitLock = elevator.getExitLock();
        Condition joinCondition = elevator.getJoinCondition();
        Condition exitCondition = elevator.getExitCondition();

        latch = generateLatch(elevatorContainer);

        exitLock.lock();
        exitCondition.signalAll();
        exitLock.unlock();

        latch.await();

        joinLock.lock();
        joinCondition.signalAll();
        joinLock.unlock();
    }
}
