package by.epam.training.validators;

import by.epam.training.beans.Elevator;
import by.epam.training.beans.Floor;
import by.epam.training.beans.House;
import by.epam.training.beans.Passenger;
import by.epam.training.exceptions.ValidatorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

/**
 * Package: by.epam.training.validators
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 19.01.2017
 */

@Component
@DependsOn({"elevator", "floors", "passengers"})
public class Validator {

    private Elevator elevator;
    private List<Floor> floors;
    private List<Passenger> passengers;

    @Autowired
    public Validator(House house) {
        validateConstructorParameters(house);
        this.elevator = house.getElevator();
        this.floors = house.getFloors();
        this.passengers = house.getPassengers();
    }

    private void validateConstructorParameters(House house) throws ValidatorException{
        if (Objects.isNull(house)) {
            throw new ValidatorException("House shouldn't be a null");
        }
        if (Objects.isNull(house.getElevator())) {
            throw new ValidatorException("Elevator shouldn't be a null");
        }
        if (Objects.isNull(house.getPassengers())) {
            throw new ValidatorException("Passengers shouldn't be a null");
        }
        if (Objects.isNull(house.getFloors())) {
            throw new ValidatorException("Floors shouldn't be a null");
        }
    }

    private boolean validateFloors(List<Floor> floors) {
        if (Objects.isNull(floors)) {
            throw new ValidatorException("Floors is null", new NullPointerException());
        }
        for (Floor floor : floors) {
            if (!floor.isEmptyDispatch()) {
                return false;
            }
        }
        return true;
    }

    private boolean validateElevator(Elevator elevator) {
        if (Objects.isNull(elevator)) {
            throw new ValidatorException("Elevator is null", new NullPointerException());
        }
        return elevator.getElevatorContainer().isEmpty();
    }

    private boolean validatePassengers(List<Passenger> passengers) {
        if (Objects.isNull(passengers)) {
            throw new ValidatorException("Passengers is null", new NullPointerException());
        }
        for (Passenger passenger : passengers) {
            if (passenger.getStatement() != Passenger.Statement.COMPLETED) {
                return false;
            }
        }
        return true;
    }

    public boolean validate() {
        return validateFloors(floors) &&
                    validateElevator(elevator) &&
                        validatePassengers(passengers);
    }
}
