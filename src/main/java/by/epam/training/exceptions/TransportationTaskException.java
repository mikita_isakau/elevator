package by.epam.training.exceptions;

/**
 * Package: by.epam.training.exceptions
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 15.01.2017
 */
public class TransportationTaskException extends RuntimeException {
    public TransportationTaskException() { }

    public TransportationTaskException(String message) {
        super(message);
    }

    public TransportationTaskException(String message, Throwable cause) {
        super(message, cause);
    }

    public TransportationTaskException(Throwable cause) {
        super(cause);
    }
}
