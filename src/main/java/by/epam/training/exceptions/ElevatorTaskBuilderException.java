package by.epam.training.exceptions;

/**
 * Package: by.epam.training.exceptions
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 15.01.2017
 */
public class ElevatorTaskBuilderException extends RuntimeException {

    public ElevatorTaskBuilderException() {}

    public ElevatorTaskBuilderException(String message) {
        super(message);
    }

    public ElevatorTaskBuilderException(String message, Throwable cause) {
        super(message, cause);
    }

    public ElevatorTaskBuilderException(Throwable cause) {
        super(cause);
    }
}
