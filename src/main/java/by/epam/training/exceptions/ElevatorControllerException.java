package by.epam.training.exceptions;

/**
 * Package: by.epam.training.exceptions
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 15.01.2017
 */
public class ElevatorControllerException extends RuntimeException {

    public ElevatorControllerException() {}

    public ElevatorControllerException(String message) {
        super(message);
    }

    public ElevatorControllerException(String message, Throwable cause) {
        super(message, cause);
    }

    public ElevatorControllerException(Throwable cause) {
        super(cause);
    }
}
