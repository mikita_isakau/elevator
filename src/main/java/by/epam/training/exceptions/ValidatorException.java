package by.epam.training.exceptions;

/**
 * Package: by.epam.training.exceptions
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 18.02.2017
 */
public class ValidatorException extends RuntimeException {
    public ValidatorException(String message) {
        super(message);
    }

    public ValidatorException(String message, Throwable cause) {
        super(message, cause);
    }
}
