package by.epam.training;

import by.epam.training.beans.AppLogger;
import by.epam.training.beans.Floor;
import by.epam.training.beans.House;
import by.epam.training.beans.Passenger;
import by.epam.training.constants.Result;
import by.epam.training.controllers.ElevatorController;
import by.epam.training.tasks.ElevatorTask;
import by.epam.training.tasks.TransportationTask;
import by.epam.training.validators.Validator;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.Supplier;

/**
 * Package: PACKAGE_NAME
 * Project name: ElevatorTask
 * Created by Mikita Isakau
 * Creation date: 31.01.2017
 */

public class Runner {

    private enum Contexts {
        ANNOTATION_CONTEXT {
            @Override
            public ConfigurableApplicationContext getContext() {
                return new AnnotationConfigApplicationContext(CONTEXT_CLASS);
            }
        }, XML_CONTEXT {
            @Override
            public ConfigurableApplicationContext getContext() {
                return new ClassPathXmlApplicationContext(CONFIG_LOCATION);
            }
        };

        private static final Class<Config> CONTEXT_CLASS = Config.class;
        private static final String CONFIG_LOCATION = "beans.xml";

        public abstract ConfigurableApplicationContext getContext();
    }

    private static final Supplier<ConfigurableApplicationContext> CONTEXT_SUPPLIER
            = Contexts.ANNOTATION_CONTEXT::getContext;
    private static final ConfigurableApplicationContext APPLICATION_CONTEXT = CONTEXT_SUPPLIER.get();

    private static final ExecutorService EXECUTOR_SERVICE =
            APPLICATION_CONTEXT.getBean(ExecutorService.class);
    private static final House HOUSE =
            APPLICATION_CONTEXT.getBean(House.class);
    private static final AppLogger LOGGER =
            APPLICATION_CONTEXT.getBean(AppLogger.class);
    private static final ElevatorTask ELEVATOR_TASK =
            APPLICATION_CONTEXT.getBean(ElevatorTask.class);
    private static final List<TransportationTask> TASKS =
            APPLICATION_CONTEXT.getBean("transportationTasks", List.class);
    private static final Validator VALIDATOR =
            APPLICATION_CONTEXT.getBean(Validator.class);

    public static void main(String[] args) {
        LOGGER.log("_________________");
        LOGGER.log("STARTING PROGRAM:");
        LOGGER.log("_________________");
        viewInfo(HOUSE, LOGGER);
        showContainersInLog(HOUSE, LOGGER);
        submitTasks(EXECUTOR_SERVICE, TASKS);
        Future<Result> elevatorFuture =  EXECUTOR_SERVICE.submit(ELEVATOR_TASK);

        try {
            Result result = elevatorFuture.get();
            LOGGER.log("-------------------------");
            LOGGER.log("RESULT OF TRANSPORTATION:");
            if (Result.SUCCESSFULL.equals(result)) {
                showContainersInLog(HOUSE, LOGGER);
            }
        } catch (InterruptedException | ExecutionException e) {
            LOGGER.error("Exception in Runner class, cause : ", e);
        }  finally {
            EXECUTOR_SERVICE.shutdown();
        }
        LOGGER.log("Valid transportation - " + VALIDATOR.validate());
    }

    private static void submitTasks(ExecutorService service, List<TransportationTask> tasks) {
        for (TransportationTask task : tasks) {
            service.submit(task);
        }
    }

    private static void viewInfo(House house, AppLogger logger) {
        int i = 0;
        logger.log("PASSENGERS:");
        for (Passenger passenger : house.getPassengers()) {
            logger.log((++i) + ") Passenger's ID - (" + passenger.getID() + "), Source floor number - (" +
                    passenger.getSourceFloor().getFloorNumber() +"), Destination floor number - (" +
                    passenger.getDestinationFloor().getFloorNumber()+")");
        }
    }

    private static void showContainersInLog(House house, AppLogger logger) {
        logger.log("DISPATCH CONTAINER:");
        for (Floor floor : house.getFloors()) {
            logger.log(floor.getFloorNumber() + " : " + " count(" + floor.sizeDispatch() + ") " + floor.getDispatchContainer().passengersList());
        }

        logger.log("ARRIVAL CONTAINER: ");
        for (Floor floor : house.getFloors()) {
            logger.log(floor.getFloorNumber() + " : "  + " count(" + floor.sizeArrival() + ") " + floor.getArrivalContainer().passengersList());
        }
    }
}
